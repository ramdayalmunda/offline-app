import layoutComponent from "./components/common/layout/layout.js"
export default {
    components: { "Layout": layoutComponent },
    template: `<Layout />`
}