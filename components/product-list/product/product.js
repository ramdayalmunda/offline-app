const productComponent = {
    setup(){
        console.log('product component imported')
    },
    template: /*html*/`
    <div>
        <h1>Your Product</h1>

        <p>Goods can be returned while a service once delivered cannot. Most business theorists see a continuum with pure service at one endpoint and pure commodity goods at other.</p>

        <p>For example, a restaurant provides a physical good like prepared food, but also provides services in the form of ambience, the setting and cleaning of the table etc. product features are characteristics of product that describe its appearance, components, and capabilities.</p>

        <p>A product feature is a slice of marketing strategy that highlights benefits or set of benefits for that product’s end user and features should be added based on quantifiable ways that they will add value foe the product’s end user.</p>

        <p>Customer satisfies their needs and wants, by knowing the benefits of the respective products and services.</p>

        <p>The factors of satisfaction can be: actual factor or perceived factor. This satisfies what a customer needs or wants.</p>
        
    </div>
    `
}

export default productComponent