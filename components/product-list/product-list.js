const productListComponent = {
    template: /*html*/`
    <div>
        <h1>Product List</h1>

        <p>In general, a product is defined as a ‘thing produced by labor or effort’ or the ‘result of an act or a process’. In marketing, a product is anything that can be offered to a market that might satisfy a want or need.</p>

        <p>In retail, products are called merchandise. In manufacturing, products are purchased as raw material and sold as finished goods. In economics, product can be classified into goods and services.</p>

        <p>Goods are a physical product capable of being delivered to a purchaser and involve the transfer of ownership from seller to customer. Goods are items that are tangible, such as books, pens, hats, shoes etc. Services are activities provided by other people, such as doctors, lawn care workers, dentists, barbers, and waiters or online servers.</p>

        <p>A good is a consumable item that is useful to people but scarce in relation to demand, so that human effort is required to obtain it. Goods may increase their utility directly or indirectly and may be described as having marginal.</p>
        
    </div>
    `
}

export default productListComponent