const { ref, onMounted } = Vue;
const homeComponent = {
    setup(){
        onMounted( async function (){
        } )
    },
    template: /*html*/`
    <div>
        <h1>HoME </h1>

        <p>It was going to rain. The weather forecast didn't say that, but the steel plate in his hip did. He had learned over the years to trust his hip over the weatherman. It was going to rain, so he better get outside and prepare.</p>

        <p>If you can imagine a furry humanoid seven feet tall, with the face of an intelligent gorilla and the braincase of a man, you'll have a rough idea of what they looked like -- except for their teeth. The canines would have fitted better in the face of a tiger, and showed at the corners of their wide, thin-lipped mouths, giving them an expression of ferocity.</p>
        
        <p>It was difficult for him to admit he was wrong. He had been so certain that he was correct and the deeply held belief could never be shaken. Yet the proof that he had been incorrect stood right before his eyes. "See daddy, I told you that they are real!" his daughter excitedly proclaimed.</p>
    </div>
    `
}

export default homeComponent