import headerComponent from "../header/header.js"
import footerComponent from "../footer/footer.js"
const layoutComponent = {
    components: {
        "Header": headerComponent,
        "Footer": footerComponent,
    },
    template: /*html*/`
    <Header />
    <RouterView />
    <Footer />
    `
}

export default layoutComponent