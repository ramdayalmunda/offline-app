const headerComponent = {
    template: /*html*/`
        <h2>Header Component Loaded</h2>
        <routerLink to="/">Home</routerLink>
        <span>&nbsp;</span>
        <routerLink to="/about">About</routerLink>
        <span>&nbsp;</span>
        <routerLink to="/help">Help</routerLink>
        <span>&nbsp;</span>
        <routerLink to="/product-list">Product List</routerLink>
        <span>&nbsp;</span>
        <routerLink to="/product">Product</routerLink>
    `
}

export default headerComponent