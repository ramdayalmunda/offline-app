const helpComponent = {
    template: /*html*/`
    <div>
        <h1>Help Component</h1>

        <p>Helping others is an act of spreading happiness around the world which in turn makes you happy. This gesture also shows humanity and kindness. When you help others in any form, you have many chances to get help when you are stuck somewhere. Make sure while helping others, your intention should be pure, and you should be sure to help others.</p>

        <p>Helping others is an action of kindness and humanity that spreads happiness to everyone in this world. Some people help others get help when they are stuck in a problem, and some help others as they want to have positive vibes in their lives.</p>

        <p>When you help others, your intention should be good; when you help others, do not wait for a thank you from them. Actions speak more than words; people give a speech to the world, but rather than speaking, one should help others in this world. Even small acts also play a significant role in changing one’s life.</p>


    </div>
    `
}

export default helpComponent