const filesToCache = [
    "./app.js",
    "./index.html",
    "./favicon.ico",
    "./favicon126.png",
    "./favicon256.png",
    "./manifest.json",

    //assets
    "./assets/css/bootstrap.min.css",
    "./assets/css/bootstrap.min.css.map",
    "./assets/css/custom.css",
    "./assets/css/toastify.css",
    "./assets/js/bootstrap.bundle.min.js",
    "./assets/js/bootstrap.bundle.min.js.map",
    "./assets/js/moment.min.js",
    "./assets/js/toastify.min.js",
    "./assets/js/vue-router.global.js",
    "./assets/js/vue.global.prod.js",

    //config
    "./config/router.js",
    "./config/toastify.js",

    //components
    //common
    "./components/common/footer/footer.js",
    "./components/common/header/header.js",
    "./components/common/layout/layout.js",

    //routes components
    "./components/about/about.js",
    "./components/home/home.js",
    "./components/help/help.js",
    "./components/product-list/product-list.js",
    "./components/product-list/product/product.js",

]
const routesToCache = [
    "./",
    "./about",
    "./home",
    "./help",
    "./product-list",
    "./product",
]

const cacheName = "v1"

self.addEventListener('install', (e) => {
    console.log('SW installing')
    e.waitUntil(
        caches.keys()
            .then((cacheKeys) => {
                return Promise.all(
                    cacheKeys.map(cacheKey => {
                        if (cacheKey !== cacheName) return caches.delete(cacheKey)
                    })
                )
            })
            .then(() => self.skipWaiting())
    )
    console.log('SW installed')
})

self.addEventListener('activate', (e) => {
    console.log('SW activating')
    e.waitUntil(
        caches
            .open(cacheName)
            .then(
                cache => {
                    cache.addAll(filesToCache)
                    cache.addAll(routesToCache)
                }
            )
            .then(() => self.skipWaiting())
    )
    console.log('SW activated')
    return self.clients.claim()
})
self.addEventListener('fetch', (e) => {
    console.log('fetching')
    e.respondWith(
        fetch(e.request)
            .then(res => {
                console.log('fetching online')
                return res
            })
            .catch(async (res) => {
                console.log('fetching from cache')
                return await caches
                    .match(e.request)
                    .then(res => res)
            })
    )
})