const routes = [
    { path: '/', name:'Home', component: ()=>import("../components/home/home.js") },
    { path: '/about', name:'About', component: ()=>import("../components/about/about.js") },
    { path: '/help', name:'Help', component: ()=>import("../components/help/help.js") },
    { path: '/product-list', name:'Product List', component: ()=>import("../components/product-list/product-list.js") },
    { path: '/product', name:'Product', component: ()=>import("../components/product-list/product/product.js") },
]

const router = VueRouter.createRouter({
    routes,
    history:VueRouter.createWebHistory()
})

export default router