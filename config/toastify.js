const toast = Toastify({
    text: "Hello World!"
})

/**
 * Returns the toast object after configutring it to the user's preference.
 * When no parameters are given the toast configuration remains unchanged.
 *
 * @param options Text that will be displayed on the toast.
 * @param options.text Text that will be displayed on the toast.
 * @param options.position Place where the toast will be displayed at.
 *
 * @example
 * toast.configureToast("hello!") // return toast with text "hello!".
 * toast.configureToast({text: "hello!"}) // return toast with text "hello!".
 *
 * @since 5.5.0
 */
toast.configureToast = function (options = {
    text: "Hello World!",
    position: "bottom-left",
    autoClose: 100000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "dark",
}) {
    console.log('options', options)
    if (typeof(options)=='string'){
        console.log('was string')
        this.options.text = options
    }else if (typeof(options)=='object'){
        console.log('was object')
        this.options = {
            ...this.options,
            ...options
        }
    }
    return this
}

export {
    toast,
}